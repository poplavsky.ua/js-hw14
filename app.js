/*
Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
- Взяти будь-яке готове домашнє завдання з HTML/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки
*/

const btnTheme = document.createElement('button');
btnTheme.id = "btn-theme";
btnTheme.textContent = "Змінити тему";
console.log(btnTheme);
const ulHeader = document.querySelector('.page-header-menu');
ulHeader.after(btnTheme);

btnTheme.onclick = function () {
  let theme = document.getElementById("theme");
  if (theme.getAttribute('href') == "./css/light-mode.css") {
    theme.href = "./css/dark-mode.css";
    localStorage.setItem('myKey', 'dark');
  } else {
    theme.href = "./css/light-mode.css";
    localStorage.setItem('myKey', 'light');
  }
}

let activeTheme = localStorage.getItem('myKey');
if(activeTheme === null || activeTheme === 'light') {
  theme.href = "./css/light-mode.css";
} else if (activeTheme === 'dark') {
  theme.href = "./css/dark-mode.css";
}